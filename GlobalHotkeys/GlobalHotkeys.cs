﻿/*
 * This program is able to capture keyboard events from the background.
 * This program creates hidden Form with system tray icon and actions
 * registeres native keyboard events and process them in NativeWindow's WndProc
 * program loop.
 */
using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace GlobalHotkeys
{
    public class AppContext : Form
    {
        [STAThread]
        static void Main()
        {
            Application.Run(new AppContext());
        }

        HotkeysNativeWindowHook nativeWindow;
        private NotifyIcon trayIcon;
        private ContextMenu trayMenu;

        public AppContext()
        {
            trayMenu = new ContextMenu();
            trayMenu.MenuItems.Add("Configure", OnConfigure);
            trayMenu.MenuItems.Add("Exit", OnExit);

            trayIcon = new NotifyIcon();
            trayIcon.Text = "Keyboard Global Listener";
            trayIcon.Icon = new Icon(
                System.Reflection.Assembly.GetExecutingAssembly().
                    GetManifestResourceStream("GlobalHotkeys.appIcon.ico"));
            trayIcon.ContextMenu = trayMenu;
            trayIcon.Visible = true;

            nativeWindow = new HotkeysNativeWindowHook();
            
            // Modifier keys codes: Alt = 1, Ctrl = 2, Shift = 4, Win = 8
            // Compute the addition of each combination of the keys you want to be pressed
            // ALT+CTRL = 1 + 2 = 3 , CTRL+SHIFT = 2 + 4 = 6...
            NativeMethods.RegisterHotKey(nativeWindow.Handle, HotkeyIDs.MYACTION_HOTKEY_ID1, 6, (int)Keys.Up);
            NativeMethods.RegisterHotKey(nativeWindow.Handle, HotkeyIDs.MYACTION_HOTKEY_ID2, 6, (int)Keys.Down);
        }

        protected override void OnLoad(EventArgs e)
        {
            Visible = false;
            ShowInTaskbar = false;
            base.OnLoad(e);
        }

        private void OnConfigure(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void OnExit(object sender, EventArgs e)
        {
            NativeMethods.UnregisterHotKey(this.Handle, HotkeyIDs.MYACTION_HOTKEY_ID1);
            NativeMethods.UnregisterHotKey(this.Handle, HotkeyIDs.MYACTION_HOTKEY_ID2);
            Application.Exit();
        }
    }
}
