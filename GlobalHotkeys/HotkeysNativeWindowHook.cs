﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GlobalHotkeys
{
    public sealed class HotkeysNativeWindowHook : NativeWindow, IDisposable
    {
        private const int APPCOMMAND_VOLUME_MUTE = 0x80000;
        private const int APPCOMMAND_VOLUME_UP = 0xA0000;
        private const int APPCOMMAND_VOLUME_DOWN = 0x90000;
        private const int WM_APPCOMMAND = 0x319;
        private const int WM_HOTKEY = 0x0312;

        public HotkeysNativeWindowHook()
        {
            CreateHandle(new CreateParams());
        }

        protected override void WndProc(ref Message m)
        {
            int actionID = m.WParam.ToInt32();
            if (m.Msg == WM_HOTKEY)
            {
                // My hotkey has been typed
                if (actionID == HotkeyIDs.MYACTION_HOTKEY_ID1) 
                {
                    IncreaseVolume();
                }
                else if (actionID == HotkeyIDs.MYACTION_HOTKEY_ID2) 
                {
                    DecreaseVolume();
                }
            }
            base.WndProc(ref m);
        }

        private void DecreaseVolume()
        {
            NativeMethods.SendMessageW(this.Handle, WM_APPCOMMAND, this.Handle, (IntPtr)APPCOMMAND_VOLUME_DOWN);
        }

        private void IncreaseVolume()
        {
            NativeMethods.SendMessageW(this.Handle, WM_APPCOMMAND, this.Handle, (IntPtr)APPCOMMAND_VOLUME_UP);
        }

        public void Dispose()
        {
            DestroyHandle();
        }
    }
}
